#!/bin/bash

rm -rf testout

rm -f generator.txt
rm -f compile.txt
rm -r experiment.txt

java -cp lib/parser*.jar at.pegasos.formula.Formula2Class --input tutorial.in > generator.txt

javac testout/at/test/*.java -cp lib/computer*.jar >> compile.txt 2>> compile.txt
javac testout/at/user/*.java -cp lib/computer*.jar >> compile.txt 2>> compile.txt

java -cp testout:lib/guava-28.1-jre.jar:lib/javassist-3.26.0-GA.jar:lib/reflections-0.9.10.jar:lib/jcommander-1.7.jar:lib/computer-0.0.1.jar at.pegasos.experiment.CSVExperiment --user test > experiment.txt
